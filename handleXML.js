// renameElement tests:
/*-----------------*
  TEST CASE ONE:
  - NO CHILDREN
  - NO ATTRIBUTES
*------------------*/
var test_one = $("<element/>");
var test_one_result = renameElement(test_one, 'new_element');
console.log("Test One expected result: 'NEW_ELEMENT'");
console.log("Test One actual result: " + test_one_result.prop('tagName'));
test_one_result.prop('tagName') === "NEW_ELEMENT"? console.log("Test PASSED") : console.log("Test FAILED");



/*-----------------*
  TEST CASE TWO:
  - ONE CHILD
  - NO ATTRIBUTES
*------------------*/
var test_two = $("<element/>");
test_two.append("<child>Test!</child>");
var test_two_result = renameElement(test_two, 'new_element');
console.log("Test Two expected name result: NEW_ELEMENT");
console.log("Test Two actual name result: " + test_two_result.prop('tagName'));
console.log("Test Two expected children length: 1");
console.log("Test Two actual children length: " + test_two_result.children().length);
test_two_result.prop('tagName') === "NEW_ELEMENT" && test_two_result.children().length === 1? console.log("Test PASSED") : console.log("Test FAILED");



/*-----------------*
  TEST CASE THREE:
  - NO CHILDREN
  - HAS ATTRIBUTE
*------------------*/
var test_three = $("<element occ='1'/>");
var test_three_result = renameElement(test_three, 'new_element');
console.log("Test Three expected result: 'NEW_ELEMENT'");
console.log("Test Three actual result: " + test_three_result.prop('tagName'));
console.log("Test Three expected occurrence attribute: 1");
console.log("Test Three actual result: " + test_three_result.attr('occ'));
test_three_result.prop('tagName') === "NEW_ELEMENT" && test_three_result.attr('occ') === "1"? console.log("Test PASSED") : console.log("Test FAILED");


/*
  renameElement

  params:
    - element : jQuery object containing XML
    - new_name : String with a new name to rename `element` to.

  returns:
    - new_element : A jQuery object identical to `element` in all ways but name

  usage:
    - renameElement(element, 'new_name');
*/

function renameElement(element, new_name) {
  // Create a new element with the new name:
  var new_element = $("<"+new_name+"/>");

  // Check if the existing element has any occurrence attributes:
  if (typeof element.attr('occ') != "undefined") {
    new_element.attr('occ', element.attr('occ'));
  }

  // Check for children:
  if (element.children().length > 0) {
    new_element.append(element.children());
  } else {
    new_element.text(element.text());
  }

  return new_element;
}


/*------------------------------------------------------------------------------------------------------------*/

