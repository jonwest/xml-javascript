// XML tests
var test_four_xml = $("#test_four");
var test_four_map = [
  {
    'key' : 'element',
    'value' : 'new_element' 
  },
  {
    'key' : 'child',
    'value' : 'new_child'
  },
  {
    'key' : 'grandchild',
    'value' : 'new_grandchild'
  }
];

var remapped = $("<remapped/>");

$(test_four_map).each(function(){
  if (remapped.find(this.key).length > 0) {

  } else {
    remapped.append(test_four_xml.find(this.key));
  }
});

// now remapped is good to go
function remap(parent, map) {
  $(map).each(function(){
    var temp = renameElement($(parent).find(this.key), this.value);
    $(parent).find(this.key).parent().append(temp);
    $(parent).find(this.key).remove();
  });

  return parent;
}
